<?php

class Name {

    const DEFAULT_LENGTH = 6;

    public function checkStringLength($name){
        return (strlen($name)>=self::DEFAULT_LENGTH)?TRUE:FALSE;
    }

}
